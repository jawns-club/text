* We do not share or sell any of your private information, ever. 
* We do have access to your email address in the event of needing to contact you directly about your account, but we will never send unsolicited marketing messages. 
* DMs are not encrypted, so we recommend against sharing personal/sensitive information via DM. If encrypted DMs becomes an option in the future, we will enable it. In the mean time, your admins and moderators will not creep in your DMs. 
* You may request a full deletion of your account at any time. Send requests to <a href="mailto:jawns@indyhall.org">jawns@indyhall.org</a>.
