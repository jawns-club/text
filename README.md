# Community Guidelines, Rules, and Messaging

The files in this repo mirror the guidelines and rules that govern the jawns.club server, and archive the text/html/formatting that we paste into our server's Mastodon settings. 

Thes files include:
- Extended server description, [which includes our full Code of Conduct](https://codeberg.org/jawns-club/text/src/branch/main/extended-description.html) found on https://jawns.club/about
- Plain-language [Privacy Policy](https://codeberg.org/jawns-club/text/src/branch/main/privacy-policy.md)
- Bullet-point [server rules](https://codeberg.org/jawns-club/text/src/branch/main/server-rules.md)
- Short [server description](https://codeberg.org/jawns-club/text/src/branch/main/server-description.md) that appears in server directories
- The html + language to [indicate that our server is semi-open](https://codeberg.org/jawns-club/text/src/branch/main/registrations.html) and requires an invite by request
- Our [custom CSS Styles](https://codeberg.org/jawns-club/text/src/branch/main/custom.css) and overrides

Updates to this repo are manual, so it's possible that there is a delay between updates on the live site being reflected here. 

## Support jawns.club
This server is run and moderated as a community service by volunteers, and funded entirely by members like you. Consider [making a recurring donation](https://liberapay.com/skyfaller/) to help us keep the lights on. 

## Coming Soon
- Moderator Team Criteria
- Moderation Workflows

