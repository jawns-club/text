1. Anyone in, near, or who just loves Philadelphia is welcome here.
2. Have fun, be weird, but keep it respectful of others.
3. Absolutely no racism, sexism, homophobia, transphobia, antisemitism, etc allowed.
4. No trolling and/or personal attacks.
5. No intentional misinformation.
6. No Nazis or any other forms of white supremacy allowed.
7. You can share your work but absolutely no crypto, scams, or MLMs.
8. Be a good citizen, report posts that break the rules.
